# UE5_MultiplayerSessionPlugin



## Getting started

Adds multiplayer capabilities for Unreal5 using Steam sessions

Steps
- Into Plugins paste the UE5_Multiplayer folder
- Enable Online Subsystem Steam in your project
- Add config in DefaultEngine.ini
	https://docs.unrealengine.com/5.2/en-US/online-subsystem-steam-interface-in-unreal-engine/
	
[/Script/Engine.GameEngine]
+NetDriverDefinitions=(DefName="GameNetDriver",DriverClassName="OnlineSubsystemSteam.SteamNetDriver",DriverClassNameFallback="OnlineSubsystemUtils.IpNetDriver")

[OnlineSubsystem]
DefaultPlatformService=Steam

[OnlineSubsystemSteam]
bEnabled=true
SteamDevAppId=480
bInitServerOnClient=true

; If using Sessions
; bInitServerOnClient=true (for unreal 5)

[/Script/OnlineSubsystemSteam.SteamNetDriver]
NetConnectionClassName="OnlineSubsystemSteam.SteamNetConnection"

- Add default maxplayer to config in DefaultGame.ini
[/Script/Engine.GameSession]
MaxPlayer=100

- Add menu widget for simple UI
	In the start game scene blueprint : event BeginPlay -> Create widget with class WBP_Menu, return value -> Menu SetUp
	Reference in the lobby path the map where to travel to once connected

- Before packaging, make sure to include all maps for packaging in the project settings